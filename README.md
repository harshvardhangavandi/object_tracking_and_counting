* [Install Object Detection API](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2.md#installation)
* Clone the repository
>git clone https://harshvardhangavandi@bitbucket.org/harshvardhangavandi/object_tracking_and_counting.git

* Installing Dependencies
>cd object_tracking_and_counting
>
>pip install -r requirements.txt
* Download CMake from [cmake](https://cmake.org/download/) and add it to PATH
* Install cmake and dlib
>pip install cmake
>pip install dlib
* To run cumulative counting with a Tensorflow object detection model use the tensorflow_object_tracking_counting.py script
* [Execution on command line](Execution.txt)
The commands stated below are to be used accordingly for the execution on command line.

To run cumulative counting with a Tensorflow object detection model use the tensorflow_object_tracking_counting.py script.

usage: tensorflow_cumulative_object_counting.py [-h] -m MODEL -l LABELMAP [-v VIDEO_PATH] [-t THRESHOLD] [-roi ROI_POSITION] [-la LABELS [LABELS ...]] [-a] [-s SKIP_FRAMES] [-sh] [-sp SAVE_PATH]

Detect objects inside webcam videostream

optional arguments:
  -h, --help            show this help message and exit
  -m MODEL, --model MODEL
                        Model Path
  -l LABELMAP, --labelmap LABELMAP
                        Path to Labelmap
  -v VIDEO_PATH, --video_path VIDEO_PATH
                        Path to video. If None camera will be used
  -t THRESHOLD, --threshold THRESHOLD
                        Detection threshold
  -roi ROI_POSITION, --roi_position ROI_POSITION
                        ROI Position (0-1)
  -la LABELS [LABELS ...], --labels LABELS [LABELS ...]
                        Label names to detect (default="all-labels")
  -a, --axis            Axis for cumulative counting (default=x axis) (Enter string ""-for horizontal axis and "True" for vertical axis) 
  -s SKIP_FRAMES, --skip_frames SKIP_FRAMES
                        Number of frames to skip between using object detection model
  -sh, --show           Show output
  -sp SAVE_PATH, --save_path SAVE_PATH
                        Path to save the output. If None output won't be saved
                        
Eg: python tensorflow_object_tracking_counting.py -m model_path/saved_model -l labelmap.pbtxt -v pedestrian.mp4 -a
